#!/bin/sh

set -e

#PKGOS-INCLUDE#

pkgos_write_new_conf () {
        local WRITE_N_CONF_PKG_NAME CONF_FNAME
        WRITE_N_CONF_PKG_NAME=${1}
        CONF_FNAME=${2}

        SRC_PATH=/usr/share/${DPKG_MAINTSCRIPT_PACKAGE}/${CONF_FNAME}
        DST_DIR=/etc/${WRITE_N_CONF_PKG_NAME}
        DST_PATH=${DST_DIR}/${CONF_FNAME}

        # Create /etc/{package}/ directory and set right owner, group and permissions
        install -g ${WRITE_N_CONF_PKG_NAME} -o ${WRITE_N_CONF_PKG_NAME} -m 0750 -d ${DST_DIR}

        if [ ! -e ${DST_PATH} ] ; then
                # Copy config to /etc/{package}/
                install -D -m 0640 -o root -g ${WRITE_N_CONF_PKG_NAME} ${SRC_PATH} ${DST_PATH}
        else
                # Set right owner, group and permissions for sure, if config already exist
                chown root:${WRITE_N_CONF_PKG_NAME} ${DST_PATH}
                chmod 0640 ${DST_PATH}
        fi
}


if [ "$1" = "configure" ] || [ "$1" = "reconfigure" ] ; then
	. /usr/share/debconf/confmodule
	. /usr/share/dbconfig-common/dpkg/postinst
	pkgos_var_user_group trove

	mkdir -p /var/cache/trove
	chown trove:trove /var/cache/trove

	pkgos_write_new_conf trove trove.conf
	pkgos_write_new_conf trove api-paste.ini
	if [ -e /usr/share/trove-common/trove-conductor.conf ]; then
		pkgos_write_new_conf trove trove-conductor.conf
	fi
	if [ -e /usr/share/trove-common/trove-taskmanager.conf ]; then
		pkgos_write_new_conf trove trove-taskmanager.conf
	fi
	mkdir -p /etc/trove/conf.d
	chown trove:trove /etc/trove/conf.d
	touch /etc/trove/conf.d/guest_info.conf
	chown trove:trove /etc/trove/conf.d/guest_info.conf

	db_get trove/configure_db
	if [ "$RET" = "true" ]; then
		pkgos_dbc_postinst /etc/trove/trove.conf database connection trove $@
		pkgos_inifile get /etc/trove/trove.conf database connection
		if [ -e /etc/trove/trove-conductor.conf ]; then
			pkgos_inifile set /etc/trove/trove-conductor.conf database connection ${RET}
		fi
		if [ -e /etc/trove/trove-taskmanager.conf ]; then
			pkgos_inifile set /etc/trove/trove-taskmanager.conf database connection ${RET}
		fi
		trove-manage db_sync
	fi
	pkgos_write_admin_creds /etc/trove/trove.conf keystone_authtoken trove

	pkgos_rabbit_write_conf /etc/trove/trove.conf oslo_messaging_rabbit trove

	db_stop
fi

#DEBHELPER#

exit 0
