#!/usr/bin/make -f

UPSTREAM_GIT := https://github.com/openstack/trove.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc,systemd

override_dh_auto_clean:
	python3 setup.py clean
	rm -f debian/trove-common.postinst
	rm -f debian/trove-common.config
	rm -f debian/trove-api.postinst
	rm -f debian/trove-api.config
	rm -f debian/*.init debian/*.service debian/*.upstart
	rm -rf debian/*.templates debian/po
	rm -rf build .stestr trove_test.sqlite *.egg-info doc/source/_static
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func trove-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func trove-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func trove-api.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func trove-api.config
	pkgos-merge-templates trove-api trove endpoint
	pkgos-merge-templates trove-common trove db rabbit ksat

override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. PYTHON=python3 python3 -m sphinx -b html doc/source debian/trove-doc/usr/share/doc/trove-doc/html
	dh_sphinxdoc
endif

override_dh_auto_install:
	echo "Do nothing..."

override_dh_auto_test:
	echo "Do nothing..."

override_dh_install:
	set -e && for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp; \
	done
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	# Because we need to delete the trove_test.sqlite db,
	# between python3.x and python3.y runs, we can't use
	# the pkgos-dh_auto_test helper.
#	export BLACKLISTED_UNIT_TESTS='trove\.tests\.unittests\.(?!(.*backup\.test_backup_models\.BackupCreateTest\.test_create_backup_cluster_instance_operation_not_supported.*|.*backup\.test_backup_models\.BackupCreateTest\.test_create_instance_not_active.*|.*backup\.test_backup_models\.BackupCreateTest\.test_create_instance_not_found.*|.*mgmt\.test_datastores\.TestDatastoreVersion\.test_version_create.*|.*guestagent\.test_redis_manager\.RedisGuestAgentManagerTest.*|.*guestagent\.test_backups\.MongodbRestoreTests\.test_restore_failed_due_to_pre_restore.*|.*guestagent\.test_backups\.MongodbRestoreTests\.test_restore_failed_due_to_run_restore.*|.*guestagent\.test_backups\.MongodbRestoreTests\.test_restore_success.*|.*backup\.test_backup_models\.BackupCreateTest\.test_create.*|.*backup\.test_backup_models\.BackupDeleteTest\.test_delete.*))'
	set -e ; set -x ; for pyvers in `py3versions -vr 2>/dev/null` ; do \
		rm -rf .stestr ; \
		find $(CURDIR) -name trove_test.sqlite -delete ; \
		stestr init ; \
		if ! PYTHON=python$$pyvers python$$pyvers -m stestr run --serial --subunit 'trove\.tests\.unittests.*' | subunit2pyunit ; then \
			pip3 freeze ; \
		fi ; \
	done
endif
	dh_install
	rm -rf $(CURDIR)/debian/tmp/usr/etc
	dh_missing

	mkdir -p $(CURDIR)/debian/trove-common/usr/share/trove-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file=$(CURDIR)/debian/trove-common/usr/share/trove-common/trove.conf \
		--wrap-width 140 \
		--namespace trove.config \
		--namespace oslo.db \
		--namespace oslo.messaging \
		--namespace oslo.middleware.cors \
		--namespace oslo.middleware.healthcheck \
		--namespace oslo.middleware.http_proxy_to_wsgi \
		--namespace oslo.log \
		--namespace oslo.policy \
		--namespace osprofiler \
		--namespace oslo.cache

	install -D -m 0664 $(CURDIR)/etc/trove/api-paste.ini $(CURDIR)/debian/trove-common/usr/share/trove-common/api-paste.ini

	mkdir -p $(CURDIR)/debian/trove-common/etc/trove/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file=$(CURDIR)/debian/trove-common/etc/trove/policy.d/00_default_policy.yaml \
		--format yaml \
		--namespace trove
override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
